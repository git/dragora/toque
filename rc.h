/*  Toque - A source builder based on recipes.
    Copyright (C) 2012-2015 Antonio Diaz Diaz.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Arg_parser;

namespace RC {

const std::vector< std::string > & order_names();
const std::vector< std::string > & package_names();
const std::vector< std::string > & recipe_names();

     // Returns true if 'name' is defined (even to the empty string).
bool get_value( const std::string & name, std::string & value,
                const Symbol_table * const symbol_tablep = 0 );

    // Returns 0 for success, 1 for file not found, 2 for syntax error.
int process_rcfile( const std::string & name );

    // Returns 0 if success, 1 if invalid variable.
int process_arguments( const Arg_parser & parser, int argind );

void reset();

} // end namespace RC
