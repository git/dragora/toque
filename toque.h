/*  Toque - A source builder based on recipes.
    Copyright (C) 2012-2015 Antonio Diaz Diaz.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Symbol_table
  {
  struct Symbol
    {
    std::string name;
    std::string value;
    Symbol( const std::string & n, const std::string & v )
      : name( n ), value( v ) {}
    };

  std::vector< Symbol > data;
  const std::string empty_value;

  bool find_( const std::string & name, int & i ) const
    {
    int l = 0, r = data.size();
    while( true )				// binary search
      {
      i = ( l + r ) / 2;
      if( l >= r ) return false;		// name not found
      const int d = name.compare( data[i].name );
      if( d == 0 ) return true;			// name found
      if( d < 0 ) r = i;			// name < data[i].name
      else l = i + 1;
      }
    }

public:
  Symbol_table() {}				// keep gcc 3.3.6 happy

  int size() const { return data.size(); }
  const std::string & name( const int i ) const { return data[i].name; }
  const std::string & value( const int i ) const { return data[i].value; }

  bool find( const std::string & name, std::string & value ) const
    {
    int i; bool found = find_( name, i );
    if( found ) value = data[i].value;
    return found;
    }

  void set( const std::string & name, const std::string & value )
    {
    int i; bool found = find_( name, i );
    if( !found ) data.insert( data.begin() + i, Symbol( name, value ) );
    else data[i].value = value;
    }

  void reset() { data.clear(); }

  void print() const
    {
    for( unsigned i = 0; i < data.size(); ++i )
      std::printf( "%s = %s\n", data[i].name.c_str(), data[i].value.c_str() );
    }
  };


struct Error
  {
  const char * const msg;
  explicit Error( const char * const s ) : msg( s ) {}
  };


// Defined in main.cc
extern int verbosity;
void maybe_append_slash( std::string & arg );
std::string my_getcwd();
void show_error( const char * const msg, const int errcode = 0,
                 const bool help = false );
std::string to_string( const long num, const unsigned width = 1 );
