/*  Toque - A source builder based on recipes.
    Copyright (C) 2012-2015 Antonio Diaz Diaz.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

#include "toque.h"
#include "rc.h"
#include "recipe.h"


Build_options build_options;

namespace {

int cant_change( const std::string & directory )
  {
  std::string msg = "Can't change directory to '" + directory + "'.";
  show_error( msg.c_str() );
  return 1;
  }

int cant_execute( const std::string & command, const int status )
  {
  std::string msg;
  if( WIFEXITED( status ) )
    msg = "Error executing '" + command + "'. Exit status = " +
          to_string( WEXITSTATUS( status ) ) + '.';
  else
    msg = "Can't execute '" + command + "'.";
  show_error( msg.c_str() );
  return 1;
  }


int components( const std::string & path )
  {
  int c = 0;
  for( unsigned i = 0; i + 1 < path.size(); ++i )
    if( path[i] == '/' && path[i+1] != '/' ) ++c;
  return c;
  }


bool empty_directory( const std::string & dir )
  {
  DIR * const dirp = opendir( dir.c_str() );
  if( !dirp ) return true;
  while( true )
    {
    const struct dirent * const entryp = readdir( dirp );
    if( !entryp ) { closedir( dirp ); break; }
    const char * const p = entryp->d_name;
    if( p && p[0] && std::strcmp( p, "." ) != 0 && std::strcmp( p, ".." ) != 0 )
      return false;
    }
  return true;
  }


bool intercept_cd( const std::string & command )
  {
  const int len = command.size();
  int i = 0;
  while( i < len && std::isspace( command[i] ) ) ++i;	// strip spaces
  if( i + 3 >= len || command[i] != 'c' || command[i+1] != 'd' ||
      !std::isspace( command[i+2] ) ) return false;
  i += 3;
  while( i < len && std::isspace( command[i] ) ) ++i;	// strip spaces
  if( i >= len ) return false;
  for( int j = i + 1; j < len; ++j )
    if( std::isspace( command[j] ) ) return false;
  if( chdir( command.substr( i ).c_str() ) != 0 ) return false;
  return true;
  }


bool remove_dir( const std::string & dir )
  {
  struct stat st;

  if( stat( dir.c_str(), &st ) == 0 && S_ISDIR( st.st_mode ) )
    {
    std::string command = "chmod -R a+rwX " + dir;	// ensure rm will work
    std::system( command.c_str() );			// ignore errors
    command = "rm -rf " + dir;
    const int status = std::system( command.c_str() );
    if( status != 0 ) { cant_execute( command, status ); return false; }
    }
  return true;
  }

} // end namespace


Recipe::Recipe( const char * const name )
  : filename_( name ), error_( false )
  {
  read_recipe();
  }


void Recipe::strip_files( const std::string & dir ) const
  {
  DIR * const dirp = opendir( dir.c_str() );
  if( !dirp ) return;

  while( true )
    {
    const struct dirent * const entryp = readdir( dirp );
    if( !entryp ) { closedir( dirp ); break; }
    const char * const p = entryp->d_name;
    if( !p || !p[0] || p[0] == '.' ) continue;
    const std::string name = dir + '/' + p;
//std::fprintf( stderr, "name = '%s'\n", name.c_str() );
    struct stat st;
    if( stat( name.c_str(), &st ) == 0 )
      {
      FILE * f;
      if( S_ISDIR( st.st_mode ) ) strip_files( name );
      else if( S_ISREG( st.st_mode ) && ( f = std::fopen( name.c_str(), "r" ) ) )
        {
        unsigned char buf[32];
        const int size = std::fread( buf, 1, sizeof buf, f );
        std::fclose( f );
        if( elf_opts.size() &&
            size >= 18 && std::memcmp( buf, "\177ELF", 4 ) == 0 &&
            ( ( buf[5] == 1 && ( buf[16] == 2 || buf[16] == 3 ) && buf[17] == 0 ) ||
              ( buf[5] == 2 && ( buf[17] == 2 || buf[17] == 3 ) && buf[16] == 0 ) ) )
          {
          // ELF executable || ELF shared object (LSB or MSB)
          const std::string command = "strip " + elf_opts + ' ' + name;
//std::fprintf( stderr, "command = '%s'\n", command.c_str() );
          std::system( command.c_str() );		// ignore errors
          }
        else if( ar_opts.size() &&
            size >= 6 && std::memcmp( buf, "!<arch>", 7 ) == 0 )
          {
          // current ar archive
          const std::string command = "strip " + ar_opts + ' ' + name;
//std::fprintf( stderr, "command = '%s'\n", command.c_str() );
          std::system( command.c_str() );		// ignore errors
          }
        }
      }
    }
  }


int Recipe::download_and_extract_tarball( const std::string & cwd,
                                          const std::string & tmpdir ) const
  {
  int status;
  struct stat st;
  std::string command, tardir, tarname;
  bool sha1sum_pending = false;

  if( !RC::get_value( "tardir", tardir, &symbol_table ) )
    { show_error( "'tardir' not defined." ); return 2; }
  if( tardir == "." ) tardir = cwd;
  if( !RC::get_value( "tarname", tarname, &symbol_table ) )
    { show_error( "'tarname' not defined." ); return 2; }
  const std::string full_tarname = tardir + '/' + tarname;
  const std::string full_sha1sum_name = full_tarname + ".sha1sum'";
  if( stat( full_tarname.c_str(), &st ) != 0 )
    {
    std::string tarpath;
    if( RC::get_value( "tarpath", tarpath, &symbol_table ) && tarpath.size() )
      {
      unsigned l = 0;
      while( l < tarpath.size() )
        {
        unsigned r = tarpath.find( ',', l );
        if( r > tarpath.size() ) r = tarpath.size();	// no ',' found
        if( r - l > 1 )					// non-empty path
          {
          std::string download, rsync;
          std::string full_tarname2( tarpath, l, r - l );
          const unsigned dirlen = full_tarname2.size();
          const bool isprotocol = full_tarname2.find( ':' ) < dirlen;
          maybe_append_slash( full_tarname2 ); full_tarname2 += tarname;
          if( verbosity >= 0 )
            std::fprintf( stderr, "Downloading '%s'.\n", full_tarname2.c_str() );
          if( isprotocol && dirlen > 8 &&
              ( full_tarname2.compare( 0, 7, "http://" ) == 0 ||
                full_tarname2.compare( 0, 8, "https://" ) == 0 ||
                full_tarname2.compare( 0, 6, "ftp://" ) == 0 ) &&
              RC::get_value( "download", download, &symbol_table ) &&
              download.size() )
            {
            command = "cd " + tardir + " && " + download + ' ' +
                      full_tarname2 + " 2> /dev/null";
            if( std::system( command.c_str() ) == 0 ) break;
            }
          else if( isprotocol && dirlen > 8 &&
                   full_tarname2.compare( 0, 8, "rsync://" ) == 0 &&
                   RC::get_value( "rsync", rsync, &symbol_table ) &&
                   rsync.size() )
            {
            command = "cd " + tardir + " && " + rsync + ' ' +
                      full_tarname2 + " 2> /dev/null";
            if( std::system( command.c_str() ) == 0 ) break;
            }
          else if( !isprotocol && dirlen > 0 )
            {
            command = "cd " + tardir + " && cp " + full_tarname2 + ' ' +
                      tarname + " 2> /dev/null";
            if( std::system( command.c_str() ) == 0 ) break;
            }
          }
        l = r + 1;
        }
      }
    if( stat( full_tarname.c_str(), &st ) != 0 )
      {
      std::string msg = "Can't stat source tar file '" + full_tarname + '\'';
      show_error( msg.c_str(), errno );
      return 1;
      }
    if( !S_ISREG( st.st_mode ) )
      {
      std::string msg = "Source tar file '" + full_tarname + "' is not a regular file.";
      show_error( msg.c_str() );
      return 1;
      }
    command = "touch '" + full_tarname + '\'';		// update timestamp
    status = std::system( command.c_str() );
    if( status != 0 ) return cant_execute( command, status );
    command = "tar -tf '" + full_tarname + '\'';	// verify source tarball
    status = std::system( command.c_str() );
    if( status != 0 ) return cant_execute( command, status );
    sha1sum_pending = true;
    }
  else
    {
    const time_t modtime = st.st_mtime;
    if( stat( full_sha1sum_name.c_str(), &st ) != 0 || modtime > st.st_mtime )
      sha1sum_pending = true;
    }

  if( sha1sum_pending )					// create sha1sum
    {
    command = "sha1sum '" + full_tarname + "' > '" + full_sha1sum_name;
    status = std::system( command.c_str() );
    if( status != 0 ) return cant_execute( command, status );
    }

  // verify sha1sum
  command = "cd " + tardir + " && sha1sum -c '" + full_sha1sum_name;
  status = std::system( command.c_str() );
  if( status != 0 ) return cant_execute( command, status );

  // extract tarball
  command = "mkdir -p " + tmpdir;
  status = std::system( command.c_str() );
  if( status != 0 ) return cant_execute( command, status );
  command = "cd " + tmpdir + " && tar -xf '" + full_tarname + '\'';
  status = std::system( command.c_str() );
  if( status != 0 ) return cant_execute( command, status );
  return 0;
  }


int Recipe::execute() const
  {
  int status;
  std::string command;
  struct stat st;

  const std::string cwd = my_getcwd();
  if( cwd.empty() ) return 1;

  // remove destdir (if it exists)
  std::string destdir;
  if( !RC::get_value( "destdir", destdir, &symbol_table ) )
    { show_error( "'destdir' not defined." ); return 2; }
  if( !remove_dir( destdir ) ) return 1;

  // remove srcdir (if it exists)
  std::string srcdir;
  if( !RC::get_value( "srcdir", srcdir, &symbol_table ) )
    { show_error( "'srcdir' not defined." ); return 2; }
  if( srcdir[0] == '/' && components( srcdir ) < 2 )
    { show_error( "'srcdir' must be at least two levels below '/'." ); return 2; }
  if( !remove_dir( srcdir ) ) return 1;

  // download (if needed) and extract source tarball (creates srcdir)
  std::string tmpdir;
  if( !RC::get_value( "tmpdir", tmpdir, &symbol_table ) )
    { show_error( "'tmpdir' not defined." ); return 2; }
  const int retval = download_and_extract_tarball( cwd, tmpdir );
  if( retval ) return retval;

  // change ownerships and permissions
  if( stat( srcdir.c_str(), &st ) == 0 && S_ISDIR( st.st_mode ) )
    {
    if( getuid() == 0 )
      { command = "chown -R 0:0 " + srcdir;
        std::system( command.c_str() ); }		// ignore errors
    command = "chmod -R u+w,go-w,a+rX-s " + srcdir;
    status = std::system( command.c_str() );
    if( status != 0 ) return cant_execute( command, status );
    }

  if( chdir( tmpdir.c_str() ) != 0 ) return cant_change( tmpdir );

  // create destdir
  command = "mkdir -p " + destdir;
  status = std::system( command.c_str() );
  if( status != 0 ) return cant_execute( command, status );

  // run commands
  for( unsigned i = 0; i < commands.size(); ++i )
    {
    if( intercept_cd( commands[i] ) ) continue;
    status = std::system( commands[i].c_str() );
    if( status != 0 ) return cant_execute( commands[i], status );
    }

  // strip files
  std::string arch, nostrip;
  if( !RC::get_value( "arch", arch, &symbol_table ) )
    { show_error( "'arch' not defined." ); return 2; }
  if( ( ar_opts.size() || elf_opts.size() ) && arch != "noarch" &&
      ( !RC::get_value( "nostrip", nostrip, &symbol_table ) || nostrip != "yes" ) )
    strip_files( destdir );

  // create .tlz package from destdir
  std::string nopkg;
  if( empty_directory( destdir ) )
    {
    std::string msg( "warning: Destdir '" ); msg += destdir;
    msg += "' is empty. Package won't be created.";
    show_error( msg.c_str() );
    }
  else if( !RC::get_value( "nopkg", nopkg, &symbol_table ) || nopkg != "yes" )
    {
    std::string dbdir, outdir, pkgname;
    if( !RC::get_value( "dbdir", dbdir, &symbol_table ) )
      { show_error( "'dbdir' not defined." ); return 2; }
    if( !RC::get_value( "outdir", outdir, &symbol_table ) )
      { show_error( "'outdir' not defined." ); return 2; }
    if( !RC::get_value( "pkgname", pkgname, &symbol_table ) || pkgname.empty() )
      {
      std::string program, version, release;
      if( !RC::get_value( "program", program, &symbol_table ) )
        { show_error( "'program' not defined." ); return 2; }
      if( !RC::get_value( "version", version, &symbol_table ) )
        { show_error( "'version' not defined." ); return 2; }
      if( !RC::get_value( "release", release, &symbol_table ) )
        { show_error( "'release' not defined." ); return 2; }
      pkgname = program + '-' + version + '-' + arch + '+' + release + ".tlz";
      }

    // create outdir, copy recipe into package
    const std::string recipe_dest = destdir + '/' + dbdir + "/recipes";
    command = "mkdir -p '" + outdir + "' '" + recipe_dest + '\'';
    status = std::system( command.c_str() );
    if( status != 0 ) return cant_execute( command, status );
    command = "cp -p '" + std::string( filename_ ) + "' '" + recipe_dest + "/'";
    status = std::system( command.c_str() );
    if( status != 0 ) return cant_execute( command, status );

    if( verbosity >= 0 )
      std::fprintf( stderr, "Creating package '%s' in '%s'...\n",
                    pkgname.c_str(), outdir.c_str() );
    if( chdir( destdir.c_str() ) != 0 ) return cant_change( destdir );
    command = "tar -c ./* | lzip -9v > '" + outdir + '/' + pkgname + '\'';
    status = std::system( command.c_str() );
    if( status != 0 ) return cant_execute( command, status );
    }

  if( chdir( cwd.c_str() ) != 0 )
    {
    std::string msg = "Can't return to the working directory '" + cwd + '\'';
    show_error( msg.c_str(), errno );
    return 1;
    }

  // remove srcdir, destdir (if they exist and --keep is not given)
  if( !build_options.keep &&
      ( !remove_dir( srcdir ) || !remove_dir( destdir ) ) ) return 1;

  return 0;
  }


void Recipe::print() const
  {
  symbol_table.print();
  std::printf( "[build]\n" );
  for( unsigned i = 0; i < commands.size(); ++i )
    std::printf( "%s\n", commands[i].c_str() );
  std::printf( "\n" );
  }
