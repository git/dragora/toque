/*  Toque - A source builder based on recipes.
    Copyright (C) 2012-2015 Antonio Diaz Diaz.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cerrno>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <sys/utsname.h>

#include "arg_parser.h"
#include "toque.h"
#include "rc.h"
#include "recipe.h"


namespace RC {

Symbol_table symbol_table_cl;			// command line
Symbol_table symbol_table_rc;			// toquerc
std::vector< std::string > order_names_;
std::vector< std::string > package_names_;
std::vector< std::string > recipe_names_;


int my_fgetc( FILE * const f )
  {
  int ch;
  bool comment = false;

  do {
    ch = std::fgetc( f );
    if( ch == '#' ) comment = true;
    else if( ch == '\n' || ch == EOF ) comment = false;
    else if( ch == '\\' && comment )
      {
      const int c = std::fgetc( f );
      if( c == '\n' ) { std::ungetc( c, f ); comment = false; }
      }
    }
  while( comment );
  return ch;
  }


// Returns the parity of escapes (backslashes) at the end of a string.
bool trailing_escape( const std::string & s )
  {
  unsigned len = s.size();
  bool odd_escape = false;
  while( len > 0 && s[--len] == '\\' ) odd_escape = !odd_escape;
  return odd_escape;
  }


// Read a line discarding comments, leading whitespace and blank lines.
// Escaped newlines are discarded.
// Returns the empty string if at EOF.
//
const std::string & my_fgets( FILE * const f, int & linenum )
  {
  static std::string s;
  bool strip = true;			// strip leading whitespace
  s.clear();

  while( true )
    {
    int ch = my_fgetc( f );
    if( strip )
      {
      strip = false;
      while( std::isspace( ch ) )
        { if( ch == '\n' ) { ++linenum; } ch = my_fgetc( f ); }
      }
    if( ch == EOF ) { if( s.size() ) { ++linenum; } break; }
    else if( ch == '\n' )
      {
      ++linenum; strip = true;
      if( trailing_escape( s ) ) s.erase( s.size() - 1 );
      else if( s.size() ) break;
      }
    else s += ch;
    }
  return s;
  }


bool isalnum_( const unsigned char ch )
  { return ( std::isalpha( ch ) || std::isdigit( ch ) || ch == '_' ); }

bool expand_variables( std::string & value,
                       const Symbol_table * const symbol_tablep,
                       const char * const filename, const int linenum )
  {
  unsigned l = 0, r, nl, nr;		// param == [l,r), name == [nl,nr)
  while( true )
    {
    if( l < value.size() ) l = value.find( '$', l );
    if( l >= value.size() ) break;			// no '$' found
    if( l + 1 < value.size() && value[l+1] == '$' )	// literal '$'
      { value.erase( ++l, 1 ); continue; }		// remove one '$'
    if( l + 1 < value.size() && value[l+1] == '{' )	// ${name} syntax
      {
      nl = l + 2; r = value.find( '}', nl );
      if( r >= value.size() )
        { if( verbosity >= 0 )
            std::fprintf( stderr, "%s %d: Syntax error: missing '}'\n",
                          filename, linenum );
          return false; }
      nr = r++;
      }
    else						// $name syntax
      {
      nl = l + 1;
      for( r = nl; r < value.size(); ++r ) if( !isalnum_( value[r] ) ) break;
      nr = r;
      }
    if( nl >= nr )
      { if( verbosity >= 0 )
          std::fprintf( stderr, "%s %d: Syntax error: missing variable name\n",
                        filename, linenum );
        return false; }
    const std::string name( value, nl, nr - nl );
    std::string new_val;
    if( !get_value( name, new_val, symbol_tablep ) )
      { if( verbosity >= 0 )
          std::fprintf( stderr, "%s %d: Undefined variable '%s'\n",
                        filename, linenum, name.c_str() );
        return false; }
    value.replace( l, r - l, new_val );
    l += new_val.size();
    }
  return true;
  }


bool parse_variable( const std::string & line, Symbol_table & symbol_table,
                     const Symbol_table * const symbol_tablep,
                     const char * const filename, const int linenum )
  {
  const int len = line.size();
  int i = 0;
  while( i < len && std::isspace( line[i] ) ) ++i;	// strip spaces
  int l = i;
  for( ; i < len && line[i] != '=' && !std::isspace( line[i] ); ++i )
    if( !isalnum_( line[i] ) )
      { if( verbosity >= 0 )
          std::fprintf( stderr, "%s %d: invalid name.\n", filename, linenum );
        return false; }
  if( l >= i )
    { if( verbosity >= 0 )
        std::fprintf( stderr, "%s %d: missing name.\n", filename, linenum );
      return false; }
  const std::string name( line, l, i - l );

  while( i < len && std::isspace( line[i] ) ) ++i;	// strip spaces
  if( i <= 0 || i >= len || line[i] != '=' )
    { if( verbosity >= 0 )
        std::fprintf( stderr, "%s %d: missing '='.\n", filename, linenum );
      return false; }
  ++i;							// skip the '='
  while( i < len && std::isspace( line[i] ) ) ++i;	// strip spaces
  l = i; i = len;
  while( l < i - 1 && std::isspace( line[i-1] ) ) --i;	// trim spaces
  std::string value( line, l, i - l );

  if( !expand_variables( value, symbol_tablep, filename, linenum ) )
    return false;
  symbol_table.set( name, value );
  return true;
  }


const std::string & get_default_arch()
  {
  static std::string arch;

  if( arch.empty() )
    {
    struct utsname u;
    if( uname( &u ) != 0 || u.machine[0] == 0 )
      throw Error( "'arch' not defined and 'uname -m' not valid." );
    arch = u.machine;
    }
  return arch;
  }

} // end namespace RC


void Recipe::add_option( std::string & opts, const std::string & new_opt )
  {
  if( opts.size() ) opts += ' ';
  opts += new_opt;
  }


bool Recipe::parse_strip_opts( const std::string & arg )
  {
  ar_opts.clear(); elf_opts.clear();

  unsigned l = 0;
  while( l < arg.size() )
    {
    unsigned r = arg.find( ',', l );
    if( r > arg.size() ) r = arg.size();		// no ',' found
    if( r - l < 1 ) break;				// empty opt
    const std::string opt( arg, l, r - l );
    if( opt == "debug" )
      { add_option( ar_opts, "--strip-debug" );
        add_option( elf_opts, "--strip-debug" ); }
    else if( opt == "sections" )
      add_option( elf_opts,
                  "--remove-section=.comment --remove-section=.note" );
    else if( opt == "unneeded" )
      add_option( elf_opts, "--strip-unneeded" );
    else break;
    if( r == arg.size() ) return true;			// no more opts
    l = r + 1;
    }
  return false;
  }


bool Recipe::fill_description( FILE * const f, int & linenum )
  {
  std::string s;

  while( true )
    {
    const int ch = std::fgetc( f );
    if( ch == '\n' || ch == EOF )
      {
      if( ch == '\n' || s.size() )
        {
        ++linenum;
        if( s == "}" ) return true;
        description.push_back( s ); s.clear();
        }
      if( ch == EOF ) return false;
      }
    else s += ch;
    }
  }


// Returns true if recipe file can be read without errors.
//
bool Recipe::read_recipe()
  {
  FILE * const f = std::fopen( filename_, "r" );
  if( !f )
    {
    if( verbosity >= 0 )
      {
      char buf[256];
      snprintf( buf, sizeof buf, "Can't open recipe file '%s': %s.",
                filename_, std::strerror( errno ) );
      show_error( buf );
      }
    error_ = true; return false;
    }
  int linenum = 0;
  bool section_found = false;
  error_ = false;

  while( !error_ )				// parse recipe
    {
    std::vector< std::string > * section = 0;
    const std::string & line = RC::my_fgets( f, linenum );
    if( line.empty() )				// EOF
      {
      if( !section_found )
        { if( verbosity >= 0 )
            std::fprintf( stderr, "%s %d: error: Unexpected End Of File\n",
                          filename_, linenum ); error_ = true; }
      break;
      }
    if( line == "build {" ) section = &commands;
    else if( line == "conflicts {" ) section = &conflicts;
    else if( line == "dependencies {" ) section = &dependencies;
    else if( line == "description {" ) section = &description;
    else				// fill symbol table
      {
      if( section_found )
        { if( verbosity >= 0 )
            std::fprintf( stderr, "%s %d: error: Unknown section name\n",
                          filename_, linenum ); error_ = true; break; }
      if( !RC::parse_variable( line, symbol_table, &symbol_table, filename_, linenum ) )
        { error_ = true; break; }
      continue;
      }
    section_found = true;
    if( !section->empty() )
      { if( verbosity >= 0 )
          std::fprintf( stderr, "%s %d: error: Repeated section\n",
                      filename_, linenum );
        error_ = true; break; }
    if( section == &description )
      {
      if( !fill_description( f, linenum ) )
        { if( verbosity >= 0 )
            std::fprintf( stderr, "%s %d: error: Unexpected End Of File\n",
                        filename_, linenum );
          error_ = true; }
      }
    else while( !error_ )				// read section
      {
      std::string line = RC::my_fgets( f, linenum );
      if( line.empty() )				// EOF
        { if( verbosity >= 0 )
            std::fprintf( stderr, "%s %d: error: Unexpected End Of File\n",
                        filename_, linenum );
          error_ = true; break; }
      if( line == "}" )
        {
        if( section->empty() )
          { if( verbosity >= 0 )
              std::fprintf( stderr, "%s %d: error: Empty section\n",
                          filename_, linenum );
            error_ = true; }
        break;
        }
      if( !RC::expand_variables( line, &symbol_table, filename_, linenum ) )
        { error_ = true; break; }
      section->push_back( line );
      }
    }
  std::fclose( f );
  std::string strip;
  if( RC::get_value( "strip", strip, &symbol_table ) &&
      strip.size() && strip != "none" )
    {
    if( !parse_strip_opts( strip ) )
      { show_error( "error: Bad value for variable 'strip'." ); error_ = true; }
    }
  return !error_;
  }


const std::vector< std::string > & RC::order_names() { return order_names_; }

const std::vector< std::string > & RC::package_names() { return package_names_; }

const std::vector< std::string > & RC::recipe_names() { return recipe_names_; }


bool RC::get_value( const std::string & name, std::string & value,
                    const Symbol_table * const symbol_tablep )
  {
  if( symbol_table_cl.find( name, value ) ) return true;
  if( symbol_tablep )				// symbol_table of recipe
    if( symbol_tablep->find( name, value ) ) return true;
  if( symbol_table_rc.find( name, value ) ) return true;

  // return default values of variables
  if( name == "arch" ) { value = get_default_arch(); return true; }
  if( name == "dbdir" ) { value = "/var/lib/toque"; return true; }
  if( name == "destdir" )
    {
    std::string tmpdir, program;
    if( !get_value( "tmpdir", tmpdir, symbol_tablep ) )
      throw Error( "'tmpdir' not defined but needed for default 'destdir'." );
    if( !get_value( "program", program, symbol_tablep ) )
      throw Error( "'program' not defined but needed for default 'destdir'." );
    value = tmpdir + "/package-" + program; return true;
    }
  if( name == "outdir" ) { value = "/var/cache/toque/packages"; return true; }
  if( name == "recipedir" ) { value = "/usr/src/toque/recipes"; return true; }
  if( name == "rootdir" ) { value = ""; return true; }
  if( name == "srcdir" )
    {
    std::string tmpdir, program, version;
    if( !get_value( "tmpdir", tmpdir, symbol_tablep ) )
      throw Error( "'tmpdir' not defined but needed for default 'srcdir'." );
    if( !get_value( "program", program, symbol_tablep ) )
      throw Error( "'program' not defined but needed for default 'srcdir'." );
    if( !get_value( "version", version, symbol_tablep ) )
      throw Error( "'version' not defined but needed for default 'srcdir'." );
    value = tmpdir + '/' + program + '-' + version; return true;
    }
  if( name == "tmpdir" ) { value = "/tmp/sources"; return true; }
  return false;
  }


    // Returns 0 for success, 1 for file not found, 2 for syntax error.
int RC::process_rcfile( const std::string & name )
  {
  FILE * const f = std::fopen( name.c_str(), "r" );
  if( !f ) return 1;

  if( verbosity >= 1 )
    { std::fprintf( stderr, "Processing '%s'... ", name.c_str() );
      std::fflush( stderr ); }

  int retval = 0;
  int linenum = 0;

  while( true )
    {
    const std::string & line = RC::my_fgets( f, linenum );
    if( line.empty() ) break;				// EOF
    const std::string msg = ( retval ? name : "\n" + name );
    if( !parse_variable( line, symbol_table_rc, 0, msg.c_str(), linenum ) )
      retval = 2;
    }
  std::fclose( f );
  if( !retval && verbosity >= 1 ) std::fprintf( stderr, "done\n" );
  return retval;
  }


    // Returns 0 if success, 1 if invalid variable.
int RC::process_arguments( const Arg_parser & parser, int argind )
  {
  for( int v = 1; argind < parser.arguments(); ++argind )
    {
    const std::string & arg = parser.argument( argind );
    if( arg.find( '=' ) >= arg.size() )			// package or recipe
      {
      if( arg.size() > 6 && arg.compare( arg.size() - 6, 6, ".order" ) == 0 )
        order_names_.push_back( arg );
      else if( arg.size() > 4 && arg.compare( arg.size() - 4, 4, ".tlz" ) == 0 )
        package_names_.push_back( arg );
      else
        recipe_names_.push_back( arg );
      continue;
      }
    if( !parse_variable( arg, symbol_table_cl, 0, "\ncommand line variable", v ) )
      return 1;
    ++v;
    }
  return 0;
  }


void RC::reset()
  {
  symbol_table_rc.reset();
  }
