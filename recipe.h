/*  Toque - A source builder based on recipes.
    Copyright (C) 2012-2015 Antonio Diaz Diaz.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Recipe				// Build data from recipe
  {
  const char * const filename_;
  Symbol_table symbol_table;
  std::string ar_opts;		// options for strip of ar archive files
  std::string elf_opts;		// options for strip of executables and shared
  std::vector< std::string > commands;
  std::vector< std::string > conflicts;
  std::vector< std::string > dependencies;
  std::vector< std::string > description;
  bool error_;

  Recipe( const Recipe & );		// declared as private
  void operator=( const Recipe & );	// declared as private

  static void add_option( std::string & opts, const std::string & new_opt );
  bool parse_strip_opts( const std::string & arg );
  bool fill_description( FILE * const f, int & linenum );
  bool read_recipe();
  void strip_files( const std::string & dir ) const;
  int download_and_extract_tarball( const std::string & cwd,
                                    const std::string & tmpdir ) const;

public:
  Recipe( const char * const name );

//  const char * filename() const { return filename_; }
  bool error() const { return error_; }
  int execute() const;
  void print() const;
  };


struct Build_options			// options for build command
  {
  bool keep;

  Build_options()
    :
    keep( false ) {}

  void reset() { *this = Build_options(); }
  };

extern Build_options build_options;
