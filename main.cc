/*  Toque - A source builder based on recipes.
    Copyright (C) 2012-2015 Antonio Diaz Diaz.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
    Exit status: 0 for a normal exit, 1 for environmental problems
    (file not found, invalid flags, I/O errors, etc), 2 to indicate a
    corrupt or invalid input file, 3 for an internal consistency error
    (eg, bug) which caused toque to panic.
*/

#include <algorithm>
#include <cerrno>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include "arg_parser.h"
#include "toque.h"
#include "rc.h"
#include "recipe.h"


namespace {

const char * const     Program_name = "Toque";
const char * const     program_name = "toque";
const char * const config_file_name = "toquerc";
const char * const     program_year = "2015";
const char * invocation_name = 0;


void show_help()
  {
  std::printf( "%s - A source builder based on recipes.\n", Program_name );
  std::printf( "\nUsage: %s <command> [options] [variables] [package|recipe]...\n", invocation_name );
  std::printf( "  'variables' are definitions in the form 'name=value'.\n"
               "\nGlobal options:\n"
               "  -h, --help             display this help and exit\n"
               "  -V, --version          output version information and exit\n"
               "\nCommands:\n"
               "  help                   show list of commands\n"
               "  build                  build packages using recipes\n"
               "  erupt                  examine packages with debugging purposes\n"
               "  install                install packages (.tlz)\n"
               "  order                  resolve the build order through .order files\n"
               "  remove                 remove packages\n"
               "  upgrade                update packages\n"
               "\nCommon options:\n"
               "  -h, --help             show options for the given command and exit\n"
               "  -N, --no-rcfile        don't read runtime configuration file\n"
               "  -q, --quiet            suppress all messages\n"
               "  -v, --verbose          be verbose (a 2nd -v gives more)\n"
               "\nOptions for command 'build':\n"
               "  -k, --keep             keep (don't delete) srcdir and destdir\n"
               "\nOptions for command 'install':\n"
               "  -f, --force            overwrite an installed package\n"
               "  -w, --warn             warn about the files that will be overwritten\n"
               "\nOptions for command 'order':\n"
               "  -w, --warn             warn about the order of dependencies\n"
               "\nExit status: 0 for a normal exit, 1 for environmental problems (file\n"
               "not found, invalid flags, I/O errors, etc), 2 to indicate a corrupt or\n"
               "invalid input file, 3 for an internal consistency error (eg, bug) which\n"
               "caused toque to panic.\n"
/*               "\nReport bugs to toque-bug@nongnu.org\n"
               "Toque home page: http://www.nongnu.org/toque/toque.html\n"*/ );
  }


void show_build_help()
  {
  std::printf( "Usage: %s build [options] [variables] <recipe_files>\n", invocation_name );
  std::printf( "\nBuild command options:\n"
               "  -h, --help             display this help and exit\n"
               "  -k, --keep             keep (don't delete) srcdir and destdir\n"
               "  -N, --no-rcfile        don't read runtime configuration file\n"
               "  -q, --quiet            suppress all messages\n"
               "  -v, --verbose          be verbose (a 2nd -v gives more)\n" );
  }


void show_install_help()
  {
  std::printf( "Usage: %s install [options] [variables] <package>.tlz...\n", invocation_name );
  std::printf( "\nOrder command options:\n"
               "  -f, --force            overwrite an installed package\n"
               "  -h, --help             display this help and exit\n"
               "  -N, --no-rcfile        don't read runtime configuration file\n"
               "  -q, --quiet            suppress all messages\n"
               "  -v, --verbose          be verbose (a 2nd -v gives more)\n"
               "  -w, --warn             warn about the files that will be overwritten\n" );
  }


void show_order_help()
  {
  std::printf( "Usage: %s order [options] [variables] <order_files>\n", invocation_name );
  std::printf( "\nOrder command options:\n"
               "  -h, --help             display this help and exit\n"
               "  -N, --no-rcfile        don't read runtime configuration file\n"
               "  -q, --quiet            suppress all messages\n"
               "  -v, --verbose          be verbose (a 2nd -v gives more)\n"
               "  -w, --warn             warn about the order of dependencies\n" );
  }


void show_version()
  {
  std::printf( "%s %s\n", program_name, PROGVERSION );
  std::printf( "Copyright (C) %s Antonio Diaz Diaz.\n", program_year );
  std::printf( "Copyright (C) %s Matias A. Fonzo.\n", program_year );
  std::printf( "License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl.html>\n"
               "This is free software: you are free to change and redistribute it.\n"
               "There is NO WARRANTY, to the extent permitted by law.\n" );
  }


void internal_error( const char * const msg )
  {
  if( verbosity >= 0 )
    std::fprintf( stderr, "%s: internal error: %s\n", program_name, msg );
  std::exit( 3 );
  }


std::string dir_prefix( std::string arg )
  {
  unsigned i = arg.size();
  while( i > 0 && arg[i-1] != '/' ) --i;
  arg.resize( i );
  return arg;
  }


bool use_it_anyway( const std::string & name )
  {
  if( !isatty( fileno( stdin ) ) ) return false;
  std::fprintf( stderr, "There are errors in '%s'. Use it anyway (y/N)? ",
                name.c_str() );
  std::fflush( stderr );
  return ( std::tolower( std::fgetc( stdin ) ) == 'y' );
  }


void maybe_process_config_file( const Arg_parser & parser )
  {
  for( int i = 0; i < parser.arguments(); ++i )
    if( parser.code( i ) == 'N' ) return;
  if( verbosity >= 0 ) std::fprintf( stderr, "\n" );
  std::string name;
  const char * p = std::getenv( "HOME" ); if( p ) name = p;
  if( name.size() )
    {
    name += "/."; name += config_file_name;
    const int retval = RC::process_rcfile( name );
    if( retval == 0 || ( retval == 2 && use_it_anyway( name ) ) ) return;
    RC::reset();
    }
  name = SYSCONFDIR; name += '/'; name += config_file_name;
  const int retval = RC::process_rcfile( name );
  if( retval == 0 || ( retval == 2 && use_it_anyway( name ) ) ) return;
  RC::reset();
  if( verbosity >= 0 )
    std::fprintf( stderr, "%s: Couldn't open '%s'.\n",
                  program_name, name.c_str() );
  }

} // end namespace


int verbosity = 0;


void maybe_append_slash( std::string & arg )
  {
  if( arg.size() && arg[arg.size()-1] != '/' ) arg += '/';
  }


std::string my_getcwd()
  {
  char buf[1024];
  if( getcwd( buf, sizeof buf ) != buf ) buf[0] = 0;
  const std::string cwd( buf );
  if( cwd.empty() )
    show_error( "Can't obtain the current working directory." );
  return cwd;
  }


void show_error( const char * const msg, const int errcode,
                 const bool help )
  {
  if( verbosity >= 0 )
    {
    if( msg && msg[0] )
      {
      std::fprintf( stderr, "%s: %s", program_name, msg );
      if( errcode > 0 )
        std::fprintf( stderr, ": %s.", std::strerror( errcode ) );
      std::fprintf( stderr, "\n" );
      }
    if( help )
      std::fprintf( stderr, "Try '%s --help' for more information.\n",
                    invocation_name );
    }
  }


std::string to_string( const long num, const unsigned width )
  {
  std::string s;
  long i = std::labs( num );

  do { s += '0' + ( i % 10 ); i /= 10; } while( i > 0 );
  if( num < 0 ) s += '-';
  if( width > s.size() ) s.append( width - s.size(), ' ' );
  std::reverse( s.begin(), s.end() );
  return s;
  }


int main( const int argc, const char * const argv[] )
  {
  enum Mode { m_none, m_build, m_install, m_order };

  const Arg_parser::Option * options = 0;
  Mode program_mode = m_none;
  std::vector< const char * > install_args;	// args to qi-install, maybe empty
  std::vector< const char * > order_args;	// args to qi-order, maybe empty
  const std::string bindir = dir_prefix( argv[0] );
  invocation_name = argv[0];

  if( argc <= 1 ) { show_help(); return 0; }		// no command given

  const Arg_parser::Option m_build_options[] =
    {
    { 'h', "help",      Arg_parser::no  },
    { 'k', "keep",      Arg_parser::no  },
    { 'N', "no-rcfile", Arg_parser::no  },
    { 'q', "quiet",     Arg_parser::no  },
    { 'v', "verbose",   Arg_parser::no  },
    {  0 ,  0,          Arg_parser::no  } };

  const Arg_parser::Option m_install_options[] =
    {
    { 'f', "force",     Arg_parser::no  },
    { 'h', "help",      Arg_parser::no  },
    { 'N', "no-rcfile", Arg_parser::no  },
    { 'q', "quiet",     Arg_parser::no  },
    { 'v', "verbose",   Arg_parser::no  },
    { 'w', "warn",      Arg_parser::no  },
    {  0 ,  0,          Arg_parser::no  } };

  const Arg_parser::Option m_order_options[] =
    {
    { 'h', "help",      Arg_parser::no  },
    { 'N', "no-rcfile", Arg_parser::no  },
    { 'q', "quiet",     Arg_parser::no  },
    { 'v', "verbose",   Arg_parser::no  },
    { 'w', "warn",      Arg_parser::no  },
    {  0 ,  0,          Arg_parser::no  } };

  { // parse operation
  const Arg_parser::Option operations[] =
    {
    { 'h', "help",      Arg_parser::no  },
    { 'V', "version",   Arg_parser::no  },
    {  0 ,  0,          Arg_parser::no  } };

  const Arg_parser parser( argv[1], ( argc > 2 ) ? argv[2] : 0, operations );
  if( parser.error().size() )				// bad operation
    { show_error( parser.error().c_str(), 0, true ); return 1; }

  if( parser.arguments() > 0 )
    {
    const int code = parser.code( 0 );
    if( code == 'h' ) { show_help(); return 0; }
    if( code == 'V' ) { show_version(); return 0; }
    if( code ) internal_error( "uncaught option." );
    const std::string & command = parser.argument( 0 );
    if( command == "help" ) { show_help(); return 0; }
    if( command == "build" )
      { program_mode = m_build; options = m_build_options; }
    else if( command == "install" )
      { program_mode = m_install; options = m_install_options; }
    else if( command == "order" )
      { program_mode = m_order; options = m_order_options; }
    }

  if( program_mode == m_none )
    {
    show_error( "error: Bad command.", 0, true );
    return 1;
    }
  } // end parse operation

  const Arg_parser parser( argc - 1, argv + 1, options );
  if( parser.error().size() )				// bad option
    { show_error( parser.error().c_str(), 0, true ); return 1; }

  int retval = 0;
  try {
    maybe_process_config_file( parser );

    int argind = 0;
    for( ; argind < parser.arguments(); ++argind )
      {
      const int code = parser.code( argind );
      if( !code ) break;				// no more options
      switch( code )					// common options
        {
        case 'N': install_args.push_back( "-N" );
                  order_args.push_back( "-N" ); continue;
        case 'q': verbosity = -1; continue;
        case 'v': if( verbosity < 4 ) { ++verbosity; } continue;
        }
//    const char * arg = parser.argument( argind ).c_str();
      switch( program_mode )
        {
        case m_none: internal_error( "invalid operation." ); break;
        case m_build:
          switch( code )
            {
            case 'h': show_build_help(); return 0;
            case 'k': build_options.keep = true; break;
            default : internal_error( "uncaught option." );
            } break;
        case m_install:
          switch( code )
            {
            case 'f': install_args.push_back( "-f" ); break;
            case 'h': show_install_help(); return 0;
            case 'w': install_args.push_back( "-w" ); break;
            default : internal_error( "uncaught option." );
            } break;
        case m_order:
          switch( code )
            {
            case 'h': show_order_help(); return 0;
            case 'w': order_args.push_back( "-w" ); break;
            default : internal_error( "uncaught option." );
            } break;
        }
      } // end process options

    retval = RC::process_arguments( parser, argind );
    if( retval ) return retval;

    for( ; argind < parser.arguments(); ++argind )
      {
      const std::string & arg = parser.argument( argind );
      if( arg.find( '=' ) < arg.size() )		// variable
        {
        if( program_mode == m_install )
          install_args.push_back( arg.c_str() );
        else if( program_mode == m_order )
          order_args.push_back( arg.c_str() );
        }
      }

    switch( program_mode )
      {
      case m_none:
        break;
      case m_build:
        if( !RC::order_names().empty() )
          { show_error( "Order names not allowed in build command.", 0, true );
            return 1; }
        if( !RC::package_names().empty() )
          { show_error( "Package names not allowed in build command.", 0, true );
            return 1; }
        if( RC::recipe_names().empty() )
          { show_error( "You must specify at least one recipe.", 0, true );
            return 1; }
        for( unsigned i = 0; i < RC::recipe_names().size(); ++i )
          {
          std::string name( RC::recipe_names()[i] );
          if( verbosity >= 1 )
            std::fprintf( stderr, "\nprocessing recipe %d of %d (%s)\n",
                          i + 1, (int)RC::recipe_names().size(), name.c_str() );
          if( name.find( '/' ) >= name.size() )		// no '/' in name
            {
            struct stat st;
            std::string recipedir;
            if( stat( name.c_str(), &st ) == 0 )	// recipe in CWD
              {
              recipedir = my_getcwd();
              if( recipedir.empty() ) { retval = 1; break; }
              }
            else
              RC::get_value( "recipedir", recipedir );	// recipe in recipedir
            if( recipedir.size() )
              { maybe_append_slash( recipedir );
                recipedir += name; name = recipedir; }
            }
          Recipe recipe( name.c_str() );
          if( recipe.error() ) { retval = 2; break; }
          if( verbosity >= 1 ) recipe.print();
          retval = recipe.execute();
          if( retval ) break;
          }
        break;
      case m_install:
        {
        if( !RC::order_names().empty() )
          { show_error( "Order names not allowed in install command.", 0, true );
            return 1; }
        if( RC::package_names().empty() )
          { show_error( "You must specify at least one package.", 0, true );
            return 1; }
        if( !RC::recipe_names().empty() )
          { show_error( "Recipe names not allowed in install command.", 0, true );
            return 1; }
        std::string command = bindir; command += "qi-install";
        for( unsigned i = 0; i < install_args.size(); ++i )
          { command += ' '; command += install_args[i]; }
        for( unsigned i = 0; i < RC::package_names().size(); ++i )
          { command += ' '; command += RC::package_names()[i]; }
        int status = std::system( command.c_str() );
        if( status != 0 ) return 1;
        } break;
      case m_order:
        { if( RC::order_names().empty() )
          { show_error( "You must specify at least one order file.", 0, true );
            return 1; }
        if( !RC::package_names().empty() )
          { show_error( "Package names not allowed in order command.", 0, true );
            return 1; }
        if( !RC::recipe_names().empty() )
          { show_error( "Recipe names not allowed in order command.", 0, true );
            return 1; }
        std::string command = bindir; command += "qi-order";
        for( unsigned i = 0; i < order_args.size(); ++i )
          { command += ' '; command += order_args[i]; }
        for( unsigned i = 0; i < RC::order_names().size(); ++i )
          { command += ' '; command += RC::order_names()[i]; }
        int status = std::system( command.c_str() );
        if( status != 0 ) return 1;
        } break;
      }
    }
  catch( Error e ) { show_error( e.msg ); retval = 1; }
  return retval;
  }
