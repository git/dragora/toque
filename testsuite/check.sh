#! /bin/sh
# check script for Toque - A source builder based on recipes.
# Copyright (C) 2012-2015 Antonio Diaz Diaz.
#
# This script is free software: you have unlimited permission
# to copy, distribute and modify it.

LC_ALL=C
export LC_ALL
objdir=`pwd`
testdir=`cd "$1" ; pwd`
TOQUE="${objdir}"/toque
framework_failure() { echo "failure in testing framework" ; exit 1 ; }

lzip -V > /dev/null 2>&1
if [ $? != 0 ] ; then echo "lzip is needed to run tests" ; exit 1 ; fi

if [ ! -f "${TOQUE}" ] || [ ! -x "${TOQUE}" ] ; then
	echo "${TOQUE}: cannot execute"
	exit 1
fi

if [ -d tmp ] ; then rm -rf tmp ; fi
mkdir tmp
cd "${objdir}"/tmp
tmpdir=`pwd`

fail=0

printf "testing toque-%s..." "$2"

"${TOQUE}" -h > /dev/null || fail=1
printf .
"${TOQUE}" -V > /dev/null || fail=1
printf .
"${TOQUE}" build -N -h > /dev/null || fail=1
printf .
"${TOQUE}" install -N -h tmpdir="${tmpdir}" > /dev/null || fail=1
printf .
"${TOQUE}" order -N -h > /dev/null || fail=1
printf .

echo
"${TOQUE}" build -N outdir="${tmpdir}"/packages tmpdir="${tmpdir}" tarpath='http://download.savannah.gnu.org/releases/lzip/lzlib',"${testdir}" download=wget nopkg=yes "${testdir}"/lzlib.recipe > /dev/null || fail=1
"${TOQUE}" build -N outdir="${tmpdir}"/packages tmpdir="${tmpdir}" tarpath='http://download.savannah.gnu.org/releases/lzip/plzip',"${testdir}" download=wget nopkg=yes "${testdir}"/plzip.recipe > /dev/null || fail=1
if [ -e packages ] ; then fail=1 ; fi

"${TOQUE}" build -qN outdir="${tmpdir}"/packages tmpdir="${tmpdir}" recipedir="${testdir}" lzlib.recipe plzip.recipe > /dev/null || fail=1
lzip -tv packages/lzlib-1.6-*+1.tlz packages/plzip-1.2-*+1.tlz || fail=1
"${TOQUE}" build -qN outdir="${tmpdir}"/packages tmpdir="${tmpdir}" recipedir="${testdir}" arch=noarch lzlib.recipe plzip.recipe > /dev/null || fail=1
lzip -tv packages/lzlib-1.6-noarch+1.tlz packages/plzip-1.2-noarch+1.tlz || fail=1

cp "${testdir}"/plzip.recipe weird.recipe
"${TOQUE}" build -qN outdir="${tmpdir}"/packages tmpdir="${tmpdir}" pkgname=weird_name.tlz weird.recipe > /dev/null || fail=1
rm weird.recipe
lzip -tv packages/weird_name.tlz || fail=1

rm "${tmpdir}"/plzip-*.sha1sum
"${TOQUE}" build -qN outdir="${tmpdir}"/packages tmpdir="${tmpdir}" recipedir="${testdir}" plzip.recipe > /dev/null || fail=1

if [ ${fail} = 0 ] ; then
	echo "tests completed successfully."
	cd "${objdir}" && rm -r tmp
else
	echo "tests failed."
fi
exit ${fail}
